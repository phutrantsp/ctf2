package com.example.ctf.model;

import java.util.List;

public class Team {
    private String code_team;
    private int sum_point;
    private int rank;
    private int amount;
    private List<User> list_member;

    public Team(String code_team, int sum_point, int rank, int amount, List<User> list_member) {
        this.code_team = code_team;
        this.sum_point = sum_point;
        this.rank = rank;
        this.amount = amount;
        this.list_member = list_member;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getSum_point() {
        return sum_point;
    }

    public void setSum_point(int sum_point) {
        this.sum_point = sum_point;
    }

    public String getCode_team() {
        return code_team;
    }

    public void setCode_team(String code_team) {
        this.code_team = code_team;
    }

    public List<User> getList_member() {
        return list_member;
    }

    public void setList_member(List<User> list_member) {
        this.list_member = list_member;
    }
}
