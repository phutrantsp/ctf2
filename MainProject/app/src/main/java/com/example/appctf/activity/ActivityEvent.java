package com.example.appctf.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.R;
import com.example.appctf.adapter.EventAdapter;
import com.example.appctf.model.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static java.lang.Thread.sleep;

public class ActivityEvent extends AppCompatActivity  {

    ListView listView;
    ArrayList<Event> arrayListEvent, arrayListEvent2;
    EventAdapter adapter_event;
    Button btn_manage, btn_account;
    Spinner spinner1, spinner2;
    String item_format,item_year;



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);


        //Spinner handle


        Spinner spinner2 = (Spinner) findViewById(R.id.year_spinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.year_array, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
//        spinner2.setOnItemSelectedListener(this);

        Spinner spinner1 = (Spinner) findViewById(R.id.format_spinner);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.format_array, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
//        spinner1.setOnItemSelectedListener(this);

        initViews();
        initListeners();
        initObjects();








       // getData("https://ctftime.org/api/v1/events/?limit=20");



    }
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),Activity_Signin.class));
    }
    public void  initViews(){
        listView=(ListView) findViewById(R.id.listviewEvent);
        btn_manage=(Button)findViewById(R.id.buttonManageTeam);
        btn_account=(Button)findViewById(R.id.buttonMyAccount);
        spinner1=(Spinner)findViewById(R.id.format_spinner);
        spinner2=(Spinner)findViewById(R.id.year_spinner);

    }
    public void initListeners(){
      //  int size=arrayListEvent.size();
        btn_manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getApplicationContext(),ActivityTeam.class);
                startActivity(intent);
            }
        });

        btn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getApplicationContext(),ActivityUserInfo.class);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.textViewTitle);
                String eventshow = textView.getText().toString();
//                Event e = arrayListEvent.get(1);
                for (int i=0;i<arrayListEvent.size();i++){
                    Event e = arrayListEvent.get(i);
                    if (e.getTitle() == eventshow){
                       // String show = "Title: "+ e.getTitle()  + "\nFormat: " + e.getFormat() + "\nURL: " + e.getUrl() + "\nBegin: " + e.getStart() + "\nFinish: " +e.getFinish() ;
                       // Toast.makeText(parent.getContext(), show, Toast.LENGTH_SHORT).show();
                        Intent intent =new Intent(getApplicationContext(),ActivityDetail.class);
                        intent.putExtra("event", e);
                        startActivity(intent);
                        break;
                    }

                }
//        int a = arrayListEvent.size();

            }
        });
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView parent, View view, int position, long id) {
                item_format = parent.getItemAtPosition(position).toString();
                if(!item_format.equals("All"))
                {
                    arrayListEvent2.clear();
                    for(int i=0;i<arrayListEvent.size();i++){
                        Event e=arrayListEvent.get(i);
                        if(e.getFormat().equals(item_format))
                            arrayListEvent2.add(e);
                    }
                    adapter_event=new EventAdapter(getBaseContext(),R.layout.adapter,arrayListEvent2);
                    listView.setAdapter(adapter_event);
                }
                else {
                    adapter_event=new EventAdapter(getBaseContext(),R.layout.adapter,arrayListEvent);
                    listView.setAdapter(adapter_event);
                }

                Toast.makeText(getBaseContext(), item_format,Toast.LENGTH_SHORT).show();
                //int a = arrayListEvent.size();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });


        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView parent, View view, int position, long id) {
                String url = "https://ctftime.org/api/v1/events/?limit=20&start=";
                item_year = parent.getItemAtPosition(position).toString();
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    Date date_start = format.parse("01-01-" + item_year);
                    Date date_end = format.parse("31-12-" + item_year);
                    long time_start = date_start.getTime() / 1000;
                    long time_end = date_end.getTime() / 1000;
                    String start = Long.toString(time_start);
                    String end = Long.toString(time_end);
                    url += start;
                    url = url + "&finish=" + end;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                arrayListEvent.clear();

                getData(url);
//                adapter_event=new EventAdapter(getBaseContext(),R.layout.adapter,arrayListEvent);
//                listView.setAdapter(adapter_event);
                spinner1.setSelection(0);
            }





            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });




    }
    public void initObjects(){
        arrayListEvent=new ArrayList<>();
        arrayListEvent2=new ArrayList<>();



    }
    public void getData(String Url)
    {

        final RequestQueue requestQueue= Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, Url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i<response.length();i++)
                        {
                            try {
                                JSONObject object=response.getJSONObject(i);
                                arrayListEvent.add(new Event(
                                        object.getString("title"),
                                        object.getString("format"),
                                        object.getString("url"),
                                        object.getString("start"),
                                        object.getString("finish")
                                ));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter_event.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

        );
        requestQueue.add(jsonArrayRequest);
    }






}

