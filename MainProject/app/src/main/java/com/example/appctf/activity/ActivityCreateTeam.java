package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.R;

import java.util.HashMap;
import java.util.Map;

public class ActivityCreateTeam extends AppCompatActivity {

    EditText e_teamName,e_country,e_code,e_university;
    Button btn_create, btn_add, btn_profile, btn_re_create;
    String Url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/createTeam.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);


        initViews();
        initListeners();
        initObjects();
    }

    public void initViews() {
        e_teamName = (EditText)findViewById(R.id.editTextTeamName);
        e_country = (EditText)findViewById(R.id.editTextCountry);
        e_university = (EditText)findViewById(R.id.editTextUniversity);
        e_code = (EditText)findViewById(R.id.editTextCode);
        btn_create = (Button)findViewById(R.id.buttonCreate);
        btn_add=(Button)findViewById(R.id.ButtonAddMember);
        btn_profile=(Button)findViewById(R.id.buttonProfile);
        btn_re_create=(Button)findViewById(R.id.buttonCreate);

    }

    public void initListeners(){
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreateTeam(Url);

            }
        });

    }
    public void initObjects(){

    }
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),ActivityTeam.class));
    }

    private void CreateTeam(String Url){
        final String teamName = e_teamName.getText().toString().trim();
        final String university = e_university.getText().toString().trim();
        final String contry = e_country.getText().toString().trim();
        final String code = e_code.getText().toString().trim();
        if(teamName.isEmpty()||university.isEmpty()|| contry.isEmpty()||code.isEmpty()){
            Toast.makeText(getApplicationContext(),"Please input full field",Toast.LENGTH_SHORT).show();
        }
        else {
            RequestQueue requestQueue= Volley.newRequestQueue(ActivityCreateTeam.this);
            StringRequest stringRequest=new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response.trim().equals("OK"))
                    {
                        Toast.makeText(ActivityCreateTeam.this,"Create team Success",Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(getApplicationContext(),ActivityUpdate.class));

                    }
                    else{
                        Toast.makeText(ActivityCreateTeam.this,response.trim(),Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> param=new HashMap<>();
                    param.put("user_id",Activity_Signin.u.getId());
                    param.put("team_name",teamName);
                    param.put("code",code);
                    param.put("university",university);
                    param.put("country",contry);

                    return param;
                }
            };
            requestQueue.add(stringRequest);
        }

    }



}
