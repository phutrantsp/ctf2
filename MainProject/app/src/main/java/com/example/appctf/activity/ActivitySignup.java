package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.R;
import com.example.appctf.model.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class ActivitySignup extends AppCompatActivity implements View.OnClickListener {
    private EditText e_name ;
    private EditText e_username;
    private EditText e_email;
    private EditText e_password;
    private EditText e_confirmpassword;
    private Button btn_register;
    private TextView t_linkLogin;
    private User user;
    String url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/insert.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initViews();
        initListeners();
        initObjects();
    }
    private void initViews(){
        e_name=(EditText)findViewById(R.id.textInputEditTextName);
        e_username=(EditText)findViewById(R.id.textInputEditTextUsername);
        e_email=(EditText)findViewById(R.id.textInputEditTextEmail);
        e_password=(EditText)findViewById(R.id.textInputEditTextPassword);
        e_confirmpassword=(EditText)findViewById(R.id.textInputEditTextConfirmPassword);
        t_linkLogin=(TextView) findViewById(R.id.textViewLinkLogin);
        btn_register=(Button)findViewById(R.id.buttonRegister);

    }
    private void initListeners(){
        btn_register.setOnClickListener(this);
        t_linkLogin.setOnClickListener(this);
    }
    private void initObjects() {
        user = new User();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.buttonRegister:
                postDataToMySql(url);
                break;

            case R.id.textViewLinkLogin:
                finish();
                break;
        }
    }
    public String md5(String plaintext) throws NoSuchAlgorithmException {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plaintext.getBytes());
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return  generatedPassword;

    }

    private void postDataToMySql(String url)
    {
        final String name=e_name.getText().toString().trim();
        final String username=e_username.getText().toString().trim();
        final String email=e_email.getText().toString().trim();
        final String password=e_password.getText().toString().trim();
        String confirm=e_confirmpassword.getText().toString().trim();

//
        if (name.isEmpty()||username.isEmpty()||email.isEmpty()||password.isEmpty()||confirm.isEmpty()){
            Toast.makeText(this,"Please enter full fields",Toast.LENGTH_SHORT).show();
            return;
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(this,"Email invalid",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!password.equals(confirm)){
            Toast.makeText(this,"Password does not match",Toast.LENGTH_SHORT).show();
            return;
        }
//
        RequestQueue requestQueue= Volley.newRequestQueue(ActivitySignup.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.trim().equals("OK"))
                {
                    Toast.makeText(ActivitySignup.this,"Register Success",Toast.LENGTH_SHORT).show();
                    finish();
                }
                else{
                    Toast.makeText(ActivitySignup.this,response.trim(),Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param=new HashMap<>();
                param.put("name",name);
                param.put("username",username);
                try {
                    param.put("password",md5(password));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                param.put("email",email);
                return param;
            }
        };
        requestQueue.add(stringRequest);


    }

}
