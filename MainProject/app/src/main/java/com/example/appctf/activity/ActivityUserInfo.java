package com.example.appctf.activity;

import android.os.Bundle;

import com.example.appctf.model.Team;
import com.example.appctf.model.User;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.TextView;

import com.example.appctf.R;

public class ActivityUserInfo extends AppCompatActivity {

    TextView textViewName, textViewEmail, textViewTeam, textViewEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        initViews();
        initListeners();
        initObjects();
        ShowTeam(Activity_Signin.u, ActivityUpdate2.team);

    }

    public void initViews() {
        textViewName=(TextView)findViewById(R.id.tvName);
        textViewEmail=(TextView)findViewById(R.id.tvEmail);
        textViewTeam=(TextView)findViewById(R.id.tvTeam);
        textViewEvent=(TextView)findViewById(R.id.tvEvent);
    }

    public void initListeners() {
    }

    public void initObjects() {
    }

    public void ShowTeam(User u, Team t){
        textViewName.setText(u.getName());
        textViewEmail.setText(u.getEmail());

        if(t.getTeam_name()==null){
            textViewTeam.setText("Let's join a team!");
        }
        else{
            textViewTeam.setText(t.getTeam_name());
        }

        if(t.getEvent_name()==null){
            textViewEvent.setText("Let's join a event");
        }
        else{
            textViewEvent.setText(t.getEvent_name());
        }



    }

}
