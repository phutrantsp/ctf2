package com.example.appctf.model;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Event implements Serializable   {
    private String title;
    private String format;
    private String url;
    private String start;
    private String finish;


    public Event(String title, String format, String url, String start, String finish) {
        this.title = title;
        this.format = format;
        this.url = url;
        this.start = start;
        this.finish = finish;
    }
    public Event(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }


}
