package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.R;
import com.example.appctf.model.Event;

import java.util.HashMap;
import java.util.Map;

public class ActivityDetail extends AppCompatActivity {

    String Url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/joinevent.php";
    Button btn_join;
    TextView textTitle, textBegin, textFinish, textURL, textFormat;
    Event eshow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initViews();
        initListeners();
        initObjects();
        Intent i = getIntent();
        eshow = (Event)i.getSerializableExtra("event");
        textTitle.setText(eshow.getTitle());
        textBegin.setText("Begin: " + eshow.getStart());
        textFinish.setText("Finish: " + eshow.getFinish());
        textURL.setText("URL: " + eshow.getUrl());
        textFormat.setText("Format: " + eshow.getFormat());

    }
    public void initViews(){
        textTitle=(TextView)findViewById(R.id.textViewTitle);
        textBegin=(TextView)findViewById(R.id.textViewBegin);
        textFinish=(TextView)findViewById(R.id.textViewFinish);
        textURL=(TextView)findViewById(R.id.textViewURL);
        textFormat=(TextView)findViewById(R.id.textViewFormat);


        btn_join=(Button)findViewById(R.id.buttonJoin);
    }
    public void initListeners(){
        btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Activity_Signin.u.getIdteam() == null)
                    Toast.makeText(getApplicationContext(),"You must have a team!!",Toast.LENGTH_SHORT).show();
                else
                    JoinEvent(Url);
                finish();
            }
        });

    }
    public void initObjects(){

    }

    public void JoinEvent(String Url)
    {
        final String title = eshow.getTitle();

        RequestQueue requestQueue= Volley.newRequestQueue(ActivityDetail.this);
        StringRequest stringRequest=new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response.trim().equals("OK"))
                    {
                        Toast.makeText(ActivityDetail.this,"Join successfully",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else{
                        Toast.makeText(ActivityDetail.this,"You had participated a other event",Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> param=new HashMap<>();
                    param.put("id_team",Activity_Signin.u.getIdteam());
                    param.put("title",title);


                    return param;
                }
            };
            requestQueue.add(stringRequest);
    }
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),ActivityEvent.class));
    }

}
