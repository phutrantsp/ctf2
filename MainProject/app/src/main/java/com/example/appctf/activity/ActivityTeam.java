package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.appctf.R;
import com.example.appctf.model.User;

public class ActivityTeam extends AppCompatActivity {

    Button btn_create;
    Button btn_add;
    Button btn_profile;
    String Url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/update_users.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        initViews();
        initListeners();
        initObjects();



        if (isExists(Activity_Signin.u) == false){
            btn_add.setEnabled(false);
            btn_profile.setEnabled(false);
        }
        else{
            btn_create.setEnabled(false);
        }
    }
    public void initViews(){
       btn_profile=(Button)findViewById(R.id.buttonProfile);
       btn_add=(Button)findViewById(R.id.buttonAddMember);
       btn_create=(Button)findViewById(R.id.buttonCreate);
    }
    public void initListeners(){
        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ActivityProfile.class);
                startActivity(intent);

            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ActivityAddMember.class);
                startActivity(intent);
            }
        });
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ActivityCreateTeam.class);
                startActivity(intent);
            }
        });
    }
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),ActivityEvent.class));
    }
    public Boolean isExists(User u){
        if(u.getIdteam()== null)
            return false;
       return true;
    }
    public void initObjects(){

    }


}
