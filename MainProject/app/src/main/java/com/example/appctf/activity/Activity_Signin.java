package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.R;
import com.example.appctf.model.User;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class Activity_Signin extends AppCompatActivity implements View.OnClickListener {
    private EditText e_username;
    private  EditText e_password;
    private Button btn_login;
    private TextView t_linkRegister;
    String url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/login.php";

    public static User u;
    public static int flag_edit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__signin);
        initViews();
        initListeners();
        initObjects();
    }
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }
    public void  initViews(){
        e_username=(EditText)findViewById(R.id.textInputEditTextUsername);
        e_password=(EditText)findViewById(R.id.textInputEditTextPassword);
        t_linkRegister=(TextView)findViewById(R.id.textViewLinkRegister) ;
        btn_login=(Button)findViewById(R.id.buttonLogin);

    }
    public void initListeners(){
        t_linkRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister = new Intent(getApplicationContext(),ActivitySignup.class);
                startActivity(intentRegister);
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postDataToMySql(url);
            }
        });
    }
    public void initObjects(){
        u = new User();
        flag_edit=0;

    }

    @Override
    public void onClick(View v) {

    }
    public void postDataToMySql(String url) {
        final int[] flag = {0};
        final String username = e_username.getText().toString().trim();
        final String password = e_password.getText().toString().trim();

        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Please enter full fields", Toast.LENGTH_SHORT).show();
            return ;
        }


        RequestQueue requestQueue = Volley.newRequestQueue(Activity_Signin.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.equals("Incorrect username or password"))
                {
                    response.trim();
                    String[] strs=response.split("-");
                    if (strs.length==4){
                        u = new User(strs[0],strs[1],strs[2],strs[3]);
                    }
                    else{
                        u = new User(strs[0],strs[1],strs[2],strs[3],strs[4]);
                    }
                    Intent intent = new Intent(getApplicationContext(),ActivityUpdate.class);
                    startActivity(intent);
                }
                else Toast.makeText(getApplicationContext(),response.trim(),Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("username", username);
                param.put("password", md5(password));
                return param;
            }
        };
        requestQueue.add(stringRequest);

    }

    private String md5(String plaintext) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plaintext.getBytes());
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return  generatedPassword;
    }



}
