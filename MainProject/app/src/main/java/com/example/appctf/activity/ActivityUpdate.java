package com.example.appctf.activity;

import android.content.Intent;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.model.User;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Toast;

import com.example.appctf.R;

import java.util.HashMap;
import java.util.Map;

public class ActivityUpdate extends AppCompatActivity {


    String url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/update_users.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        initViews();
        initListeners();
        initObjects();
        UpdateUser(url);
        startActivity(new Intent(getApplicationContext(),ActivityUpdate2.class));

    }

    public void initObjects() {

    }

    public void initListeners() {
    }

    public void initViews() {
    }

    public void UpdateUser(String url) {

        RequestQueue requestQueue= Volley.newRequestQueue(ActivityUpdate.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.equals("Not ok"))
                {
                    response.trim();
                    String[] strs=response.split("-");
                    if (strs.length==4){
                        Activity_Signin.u = new User(strs[0],strs[1],strs[2],strs[3]);
                    }
                    else{
                        Activity_Signin.u = new User(strs[0],strs[1],strs[2],strs[3],strs[4]);
                    }
                }
                else Toast.makeText(getApplicationContext(),response.trim(),Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("username", Activity_Signin.u.getUsername());

                return param;
            }
        };
        requestQueue.add(stringRequest);

    }




}
