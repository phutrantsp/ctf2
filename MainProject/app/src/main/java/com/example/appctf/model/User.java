package com.example.appctf.model;

import java.io.Serializable;

public class User implements Serializable {
    private String id;
    private String name;
    private String email;
    private String password;
    private String username;
    private String id_team=null;

    public User(String id, String name, String username,String email, String id_team ){
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.id_team= id_team;
    }
    public User(String id, String name, String username,String email){
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
    }
    public User(){}

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getUsername(){return username; }
    public void setUsername(String username){ this.username=username;}

    public String getIdteam(){return id_team;}

    public void setIdteam(String id_team) {
        this.id_team = id_team;
    }
}
