package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.R;
import com.example.appctf.model.Team;

import java.util.HashMap;
import java.util.Map;

public class ActivityProfile extends AppCompatActivity {
    Button btn_edit;
    Button btn_save;
    EditText e_teamname,e_country,e_university;
    String Url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/editteam.php";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initViews();
        initListeners();
        initObjects();
        ShowTeam(ActivityUpdate2.team);
    }

    public void initObjects() {

    }

    public void initViews(){
        btn_edit=(Button)findViewById(R.id.buttonEdit);
        btn_save=(Button)findViewById(R.id.buttonSave);
        e_teamname=(EditText)findViewById(R.id.editTextTeamName);
        e_country=(EditText)findViewById(R.id.editTextCountry);
        e_university=(EditText)findViewById(R.id.editTextUniversity);



    }
    public void initListeners(){
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                e_teamname.setEnabled(true);
                e_country.setEnabled(true);
                e_university.setEnabled(true);

            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTeam(Url);
                Activity_Signin.flag_edit=1;
            }
        });

    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),ActivityTeam.class));
    }
    public void ShowTeam(Team t){
        String teamname= t.getTeam_name();
        e_teamname.setText(teamname);
        e_university.setText(t.getUniversity());
        e_country.setText(t.getCountry());
    }
    private void EditTeam(String Url){
        final String new_teamname = e_teamname.getText().toString().trim();
        final String new_country = e_country.getText().toString().trim();
        final String new_university = e_university.getText().toString().trim();

        if(new_teamname.isEmpty()||new_country.isEmpty()||new_university.isEmpty()){
            Toast.makeText(getApplicationContext(),"Please input all fields!",Toast.LENGTH_SHORT).show();
        }
        else {
            RequestQueue requestQueue= Volley.newRequestQueue(ActivityProfile.this);
            StringRequest stringRequest=new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response.trim().equals("OK"))
                    {
                        Toast.makeText(ActivityProfile.this,"Edit Successfully!",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),ActivityUpdate2.class));
                    }
                    else{
                        Toast.makeText(ActivityProfile.this,response.trim(),Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> param=new HashMap<>();
                    param.put("id_team",Activity_Signin.u.getIdteam());
                    param.put("new_teamname",new_teamname);
                    param.put("new_country",new_country);
                    param.put("new_university",new_university);
                    return param;
                }
            };
            requestQueue.add(stringRequest);
        }
    }
}
