package com.example.appctf.model;

import java.util.List;

public class Team {
    private String id_team;
    private String team_name;
    private int amout;
    private  String event_name=null;
    private String university;
    private String country;
    private List<User> userList;


    public Team(){ }

    public Team(String id_team, String team_name, String university, String country ){
        this.id_team=id_team;
        this.team_name = team_name;
        this.university = university;
        this.country = country;

    }
    public Team(String id_team, String team_name, String university, String country, String event_name ){
        this.id_team=id_team;
        this.team_name = team_name;
        this.university = university;
        this.country = country;
        this.event_name = event_name;
    }
    public int getAmout() {
        return amout;
    }

    public void setAmout(int amout) {
        this.amout = amout;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getId_team(){return id_team;}
    public void setId_team(String id_team){this.id_team=id_team;}
}
