package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.appctf.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_link;
    private EditText e_IP;
    private  EditText e_PORT;
    public static String  IP ="";
    public static String PORT="80";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initListeners();
        initObjects();
    }

    public void initViews() {
        btn_link=(Button)findViewById(R.id.buttonLinkToSignIn);
        e_IP=(EditText)findViewById(R.id.textInputEditTextIP);
        e_PORT=(EditText)findViewById(R.id.textInputEditTextPORT);
    }
    public void initListeners(){
        btn_link.setOnClickListener(this);

    }
    public void onBackPressed() {
        finish();
        System.exit(0);
    }
    public void initObjects(){


    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.buttonLinkToSignIn)
        {
            IP = e_IP.getText().toString().trim();
            PORT= e_PORT.getText().toString().trim();

            if (IP.isEmpty() || PORT.isEmpty()) {
                Toast.makeText(this, "Please enter IP and PORT", Toast.LENGTH_SHORT).show();
                return ;
            }
            Intent intentLogin = new Intent(getApplicationContext(),Activity_Signin.class );
            startActivity(intentLogin);

        }

    }
}
