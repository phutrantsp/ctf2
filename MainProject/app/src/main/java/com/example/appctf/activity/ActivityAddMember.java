package com.example.appctf.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.R;

import java.util.HashMap;
import java.util.Map;

public class ActivityAddMember extends AppCompatActivity {


    EditText e_member;
    Button btn_addmember;
    String Url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/addmember.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        initViews();
        initListeners();
        initObjects();
    }

    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),ActivityTeam.class));
    }

    public void initViews() {
        e_member=(EditText)findViewById(R.id.EditTextAddMember);
        btn_addmember=(Button)findViewById(R.id.ButtonAddMember);
    }

    public void initListeners(){
        btn_addmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMember(Url);
            }
        });
    }

    public void initObjects(){

    }

    private void AddMember(String Url){
        final String member = e_member.getText().toString().trim();

        if(member.isEmpty()){
            Toast.makeText(getApplicationContext(),"Please input a username to add",Toast.LENGTH_SHORT).show();
        }
        else {
            RequestQueue requestQueue= Volley.newRequestQueue(ActivityAddMember.this);
            StringRequest stringRequest=new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response.trim().equals("OK"))
                    {
                        Toast.makeText(ActivityAddMember.this,"Add member successfully",Toast.LENGTH_SHORT).show();


                        finish();
                    }
                    else{
                        Toast.makeText(ActivityAddMember.this,response.trim(),Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> param=new HashMap<>();
                    param.put("id_team",Activity_Signin.u.getIdteam());
                    param.put("member",member);


                    return param;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

}
