package com.example.appctf.activity;

import android.content.Intent;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appctf.model.Team;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Toast;

import com.example.appctf.R;

import java.util.HashMap;
import java.util.Map;

public class ActivityUpdate2 extends AppCompatActivity {

    public static Team team;
    String url="http://" + MainActivity.IP +":"+MainActivity.PORT + "/Webservice/getdataTeam.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update2);
        initViews();
        initListeners();
        initObjects();

        GetDataTeam(url);

        if(Activity_Signin.flag_edit==0){
            startActivity(new Intent(getApplicationContext(),ActivityEvent.class));
        }
        else {
            startActivity(new Intent(getApplicationContext(),ActivityProfile.class));
        }

    }

    public void initObjects() {
        team = new Team();

    }

    public void initListeners() {
    }

    public void initViews() {
    }

    public void GetDataTeam(String Url){
        RequestQueue requestQueue = Volley.newRequestQueue(ActivityUpdate2.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.equals("Not OK"))
                {
                    String[] dataOfTeam = response.split("-");
                    if(dataOfTeam.length==5)
                        team = new Team(dataOfTeam[0],dataOfTeam[1],dataOfTeam[2],dataOfTeam[3], dataOfTeam[4]);
                    else
                        team = new Team(dataOfTeam[0],dataOfTeam[1],dataOfTeam[2],dataOfTeam[3]);


                }
                else Toast.makeText(getApplicationContext(),response.trim(),Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param=new HashMap<>();
                param.put("id_team",Activity_Signin.u.getIdteam());
                return param;
            }
        };
        requestQueue.add(stringRequest);
    }

}
